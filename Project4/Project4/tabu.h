#ifndef TABU_H
#define TABU_H

#include "vector"


using namespace std;


class tabu
{
    struct node
    {
        node* nextNode;
        vector<int> data;

    };

private:
    int length;
    node* current;
    node* start;
public:
    tabu();
    tabu(int);
    void setLength(int);

    void push(vector<int>);
    bool contains(vector<int>&);

    ~tabu();
};

#endif // TABU_H
