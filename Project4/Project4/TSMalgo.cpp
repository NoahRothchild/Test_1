#include "TSMalgo.h"
#include "algorithm"
#include "filemanager.h"
#include "string"
#include "tabu.h"

#define selectionSelector 1
#define crossSelector 1
#define mutationSelector 1
#define tabuSelector 1

struct Node;

TSMAlgo::TSMAlgo()
{

}

void TSMAlgo::loadPositions()
{

    fileManager manager;
    matrix = manager.positionReader("positions8.txt");

    for(int x = 0; x < matrix.size(); x++)
    {

        vector<double> inserter;
        for(int y = 0; y < matrix.size(); y++)
        {
            if(matrix[x].getName() == y+1)
            {
                inserter.push_back(0);
                continue;
            }
            inserter.push_back(distanceCalc(matrix[x], matrix[y]));
        }
        graph.push_back(inserter);
    }

    n = graph.size();
}

void TSMAlgo::savePath(string out)
{
    fileManager manager;
    manager.pathSaver(path, out);
}

//base of code from geeks for geeks, edited to check
//all routes as opposed to a guaranteed origin
//https://www.geeksforgeeks.org/traveling-salesman-problem-tsp-implementation/
void TSMAlgo::forceTSM()
{
    distance = 0;
    int V  = matrix.size();
    if(path.size() > 0)
        path.clear();


    // store minimum weight Hamiltonian Cycle.
    double min_path = INFINITY;

    for(int s = 0; s < matrix.size(); s++)
    {
        // store all vertex apart from source vertex
        vector<int> vertex;
        for (int i = 0; i < V; i++)
            if (i != s)
                vertex.push_back(i);


        do {

            // store current Path weight(cost)
            double current_pathweight = 0;

            vector<int> tempPath;
            tempPath.push_back(s+1);

            // compute current path weight
            int k = s;
            for (int i = 0; i < vertex.size(); i++) {
                current_pathweight += graph[k][vertex[i]];
                k = vertex[i];
                tempPath.push_back(k+1);
            }
            current_pathweight += graph[k][s];

            // update minimum
            if(current_pathweight < min_path)
            {
                path = tempPath;
                min_path = current_pathweight;
            }

        } while (next_permutation(vertex.begin(), vertex.end()));

        distance = min_path;
        minDistance = min_path;
    }
    return;
}


void TSMAlgo::genAlgoTSM()
{
    srand(27);
    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
    path.clear();
    distance = INFINITY;

    //initilization
    vector<vector<int>> chromosomes;
    genAlgoInitialization(chromosomes);

    //roulette pieces
    double piece = 1.00/((chromosomes.size()*(chromosomes.size()+1))/2);
    vector<double> pieces(chromosomes.size());
    for(int x = 0; x < chromosomes.size(); x++)
    {
        if(x > 0)
            pieces[x] = ((x+1) * piece) + pieces[x-1];
        else
            pieces[x] = ((x+1) * piece);
    }
    double tempDistance = INFINITY;

    //calculate distances
    vector<double> distances;
    for(int x = 0; x < chromosomes.size(); x++)
    {
        double extraTempDistance = distanceCalc(chromosomes[x]);
        distances.push_back(extraTempDistance);
        if( extraTempDistance < tempDistance)
        {
            path = chromosomes[x];
            tempDistance = extraTempDistance;
            distance = tempDistance;
        }

    }

    //create rankings of chromosomes
    vector<double> rankings;
    vector<int> indexes;
    rankings.push_back(distances[0]);
    indexes.push_back(0);
    for(int i = 1; i < chromosomes.size(); i++)
    {
        for(int j = 0; j < rankings.size(); j++)
        {
            if(rankings[j] <= distances[i])
            {
                rankings.insert(rankings.begin() + j, distances[i]);
                indexes.insert(indexes.begin() + j, i);
                break;
            }

            else if(j == rankings.size()-1)
            {
                rankings.push_back(distances[i]);
                indexes.push_back(i);
                break;
            }
        }
    }

    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
    chrono::duration<double> time = chrono::duration_cast<chrono::duration<double>>(t2 - t1);

    do
    {
        t1 = chrono::high_resolution_clock::now();
        vector<int> child;
        vector<int> child2;
        vector<int> parents;


        child.clear();
        child2.clear();
        parents.clear();

        //select
        if(selectionSelector == 0)
        {
            selectRoulette(chromosomes, pieces, parents, distances, rankings, indexes);
        }
        else if(selectionSelector == 1)
        {
            selectRandom(chromosomes, pieces, parents);
        }

        //crossover

        if(crossSelector == 0)
        {
            crossover50(child, chromosomes, chromosomes[parents[0]], chromosomes[parents[1]]);
        }
        else if(crossSelector == 1)
        {
            crossoverOrder1(child, chromosomes, chromosomes[parents[0]], chromosomes[parents[1]]);
        }

        child2.push_back(1);
        for(int x = 1; x < n; x++)
        {
            child2.push_back(child[child.size()-x]);
        }

        //mutate
        if(mutationSelector == 0)
        {
            mutationRand(child);
            mutationRand(child2);
        }
        else if (mutationSelector == 1)
        {
            mutationHalfRand(child);
            mutationHalfRand(child2);
        }


        //rerank new children with past population
        int c1 = indexes[0];
        int c2 = indexes[1];
        chromosomes[indexes[0]] = child;
        distances[indexes[0]] = distanceCalc(child);
        chromosomes[indexes[1]] = child2;
        distances[indexes[1]] = distanceCalc(child2);

        rankings.erase(rankings.begin());
        rankings.erase(rankings.begin());
        indexes.erase(indexes.begin());
        indexes.erase(indexes.begin());
        for(int x = 0; x < distances.size()-2; x++)
        {
            if(rankings[x] <= distances[c1])
            {
                rankings.insert(rankings.begin() + x, distances[c1]);
                indexes.insert(indexes.begin(), c1);
                break;
            }
            else if(x == rankings.size()-1)
            {
                rankings.push_back(distances[c1]);
                indexes.push_back(c1);

            }
        }
        for(int x = 0; x < distances.size()-1; x++)
        {
            if(rankings[x] <= distances[c2])
            {
                rankings.insert(rankings.begin() + x, distances[c2]);
                indexes.insert(indexes.begin(), c2);
                break;
            }
            else if(x == rankings.size()-1)
            {
                rankings.push_back(distances[c2]);
                indexes.push_back(c2);

            }
        }

        //find shortest chromosome found

        tempDistance = rankings[rankings.size()-1];
        distance = tempDistance;
        path = chromosomes[indexes[indexes.size()-1]];


        t2 = chrono::high_resolution_clock::now();
        time += chrono::duration_cast<chrono::duration<double>>(t2 - t1);
        //end if taking too long
        if(time > maxAllocation)
        {
            break;
        }
    }
    while((tempDistance - minDistance) > .0000000000009);

}

void TSMAlgo::genAlgoInitialization(vector<vector<int>>& chromosomes)
{

    //determine number of chromomsomes
    int population = 0;
    if(n > 6)
        population = 100;
    else if(n > 4)
        population = 10;
    else
        population = 4;


    chromosomes.resize(population);
    for(int x = 0; x < chromosomes.size(); x++)
        chromosomes[x].resize(n);

    //initialization
    vector<int> dna(n-1);
    iota(dna.begin(), dna.end(), 2);
    for(int x = 0; x < chromosomes.size(); x++)
    {
        chromosomes[x][0] = 1;
        for(int y = 1; y < n; y++)
        {
            int random = (rand() % (n-y));
            chromosomes[x][y] = dna[random];
            dna.erase(dna.begin() + random);
        }
        dna.resize(n-1);
        iota(dna.begin(), dna.end(), 2);
    }

}

void TSMAlgo::selectRoulette(vector<vector<int>> & chroms, vector<double>& pieces, vector<int>& parents, vector<double>& distances, vector<double>& rankings, vector<int>& indexes)
{

    //get 2 parents based on a mix of random probability and fitness
    int parent1;
    int parent2;
    for(int x = 0; x < 2; x++)
    {
        double r = ((double) rand() / (RAND_MAX));
        int parent= 0;
        for(int y = 0; y < pieces.size()-1; y++)
        {
            if(r > pieces[y] && r < pieces[y+1])
            {
                parent = y+1;
                break;
            }
            else if(y == 0 && r < pieces[0])
            {
                parent = y;
                break;
            }
        }

        if(x == 0)
            parent1 = parent;
        else
            parent2 = parent;
        if(parent1 == parent2)
            x = 0;
    }

    parents.push_back(indexes[parent1]);
    parents.push_back(indexes[parent2]);

}

void TSMAlgo::selectRandom(vector<vector<int>> & chroms, vector<double>& pieces, vector<int>& parents)
{
    //two random parents
    int parent1 = rand()%(chroms.size());
    int parent2 = rand()%(chroms.size());
    parents.push_back(parent1);
    parents.push_back(parent2);
}

void TSMAlgo::crossover50(vector<int> & child, vector<vector<int> > & chroms, vector<int>& parentPath1, vector<int>& parentPath2)
{
    vector<bool> include;
    for(int x = 0; x < n; x++)
    {
        include.push_back(false);
    }
    child.clear();
    child.push_back(1);
    vector<int> dna;
    dna.resize(n-1);
    iota(dna.begin(), dna.end(), 2);

    //first half of parent 1
    for(int x = 1; x < parentPath1.size()/2; x++)
    {
        child.push_back(parentPath1[x]);
        include[parentPath1[x]-1] = true;

    }
    //second half of parent 2
    for(int x = child.size(); x < n; x++)
    {
        if(include[parentPath2[x]-1] == false)
        {
            child.push_back(parentPath2[x]);
            include[parentPath2[x]-1] = true;
        }
    }

    //missing nodes
    for(int x = 1; x < include.size(); x++)
    {
        if(include[x] == false)
            child.push_back(x+1);

    }
}

void TSMAlgo::crossoverOrder1(vector<int> & child, vector<vector<int> > & chroms, vector<int> & parentPath1, vector<int> & parentPath2)
{
    //random chunk of parent 1
    int index1;
    int index2;
    do
    {
        index1 = rand()%(n-1)+1;
        index2 = rand()%(n-1)+1;
    }
    while(index1 == index2 && abs(index1-index2) < (n-1));
    vector<bool> included;
    for(int x = 0; x < parentPath1.size(); x++)
    {
        included.push_back(false);

    }
    vector<int> nonDupIndex;
    child.push_back(1);


    if(index1 < index2)
    {
        for(int x = index1; x < index2; x++)
        {
            bool found = false;
            for(int y = index1; y < index2; y++)
            {
                if(parentPath2[x] == parentPath1[y])
                {
                    found = true;
                    included[parentPath2[x]-1] = true;
                    break;
                }
            }
            if(found == false)
            {
                nonDupIndex.push_back(parentPath2[x]);
            }
            included[parentPath1[x]-1] = true;
        }



        for(int x = 1; x < index1; x++)
        {
            if(included[parentPath2[x]-1] == false)
            {
                child.push_back(parentPath2[x]);
                included[parentPath2[x]-1] = true;
            }
            else
            {
                child.push_back(nonDupIndex[0]);
                included[parentPath2[x]-1] = true;
                nonDupIndex.erase(nonDupIndex.begin());
            }
        }
        for(int x = index1; x < index2; x++)
        {
            child.push_back(parentPath1[x]);
        }
        for(int x = index2; x < parentPath2.size(); x++)
        {
            if(included[parentPath2[x]-1] == false)
            {
                child.push_back(parentPath2[x]);
                included[parentPath2[x]-1] = true;
            }
            else
            {
                child.push_back(nonDupIndex[0]);
                included[parentPath2[x]-1] = true;
                nonDupIndex.erase(nonDupIndex.begin());
            }
        }
    }
    else
    {
        for(int x = index2; x < index1; x++)
        {
            bool found = false;
            for(int y = index2; y < index1; y++)
            {
                if(parentPath2[x] == parentPath1[y])
                {
                    found = true;
                    included[parentPath2[x]-1] = true;
                    break;
                }
            }
            if(found == false)
            {
                nonDupIndex.push_back(parentPath2[x]);
            }
            included[parentPath1[x]-1] = true;
        }



        for(int x = 1; x < index2; x++)
        {
            if(included[parentPath2[x]-1] == false)
            {
                child.push_back(parentPath2[x]);
                included[parentPath2[x]-1] = true;
            }
            else
            {
                child.push_back(nonDupIndex[0]);
                included[parentPath2[x]-1] = true;
                nonDupIndex.erase(nonDupIndex.begin());
            }
        }
        for(int x = index2; x < index1; x++)
        {
            child.push_back(parentPath1[x]);
        }
        for(int x = index1; x < parentPath2.size(); x++)
        {
            if(included[parentPath2[x]-1] == false)
            {
                child.push_back(parentPath2[x]);
                included[parentPath2[x]-1] = true;
            }
            else
            {
                child.push_back(nonDupIndex[0]);
                included[parentPath2[x]-1] = true;
                nonDupIndex.erase(nonDupIndex.begin());
            }
        }
    }
}

void TSMAlgo::mutationRand(vector<int> & child)
{
    double r = ((double) rand() / (RAND_MAX));
    if(r < .15)
    {
        int swap1 = rand()%(n-2)+2;
        int swap2 = rand()%(n-2)+2;
        int temp = child[swap1];
        child[swap1] = child[swap2];
        child[swap2] = temp;
    }
}

void TSMAlgo::mutationHalfRand(vector<int> & child)
{
    double r = ((double) rand() / (RAND_MAX));
    if(r < .15)
    {
        int swap1 = rand()%(n-2)+2;
        int swap2 = child.size()-1;
        int temp = child[swap1];
        child[swap1] = child[swap2];
        child[swap2] = temp;
    }
}



void TSMAlgo::tabuTSM()
{
    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
    path.clear();
    distance = INFINITY;
    tabu tracker;

    //choose size of tabu list
    switch(n)
    {
    case 4:
        tracker.setLength(6);
        break;
    case 5:
        tracker.setLength(10);
        break;
    case 6:
        tracker.setLength(50);
        break;
    default:
        tracker.setLength(100);
    }

    vector<int> currAns;

    //initial search
    //uses greedy search
    vector<int> cities(n-1);
    iota(cities.begin(), cities.end(), 2);

    currAns.push_back(1);
    while(currAns.size() < n)
    {
        double minJump = INFINITY;
        int index = 0;
        double tempPath = 0;
        for(int x = 0; x < graph[0].size(); x++)
        {
            tempPath = graph[currAns[currAns.size()-1]-1][x];
            if(tempPath == 0)
                continue;
            if(tempPath < minJump && !contains(currAns,x+1))
            {
                index  = x;
                minJump = tempPath;
            }
        }
        currAns.push_back(index+1);
    }

    tracker.push(currAns);
    double tempDistance = distanceCalc(currAns);
    path = currAns;

    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
    chrono::duration<double> time = chrono::duration_cast<chrono::duration<double>>(t2 - t1);

    //random swithching
    if(tabuSelector == 0)
    {
        while((tempDistance - minDistance) > .0000000000009)
        {
            t1 = chrono::high_resolution_clock::now();
            vector<int> lastAns = currAns;
            int swap1 = rand()%(n-1)+1;
            int swap2 = rand()%(n-1)+1;
            int temp = currAns[swap1];
            currAns[swap1] = currAns[swap2];
            currAns[swap2] = temp;
            if(tracker.contains(currAns) == false)
            {
                tracker.push(currAns);
                double newDistance = distanceCalc(currAns);

                if(newDistance < tempDistance)
                {
                    path = currAns;
                    tempDistance = newDistance;
                }
            }
            t2 = chrono::high_resolution_clock::now();
            time += chrono::duration_cast<chrono::duration<double>>(t2 - t1);
            if(time > maxAllocation)
            {
                break;
            }
        }
    }

    //first index and random index swap
    else if(tabuSelector == 1)
    {
        while((tempDistance - minDistance) > .0000000000009)
        {
            t1 = chrono::high_resolution_clock::now();
            vector<int> lastAns = currAns;
            int swap1 = 2;
            int swap2 = rand()%(n-1)+1;
            int temp = currAns[swap1];
            currAns[swap1] = currAns[swap2];
            currAns[swap2] = temp;
            if(tracker.contains(currAns) == false)
            {
                tracker.push(currAns);
                double newDistance = distanceCalc(currAns);

                if(newDistance < tempDistance)
                {
                    path = currAns;
                    tempDistance = newDistance;
                }
            }
            t2 = chrono::high_resolution_clock::now();
            time += chrono::duration_cast<chrono::duration<double>>(t2 - t1);
            if(time > maxAllocation)
            {
                break;
            }
        }
    }

    distance = distanceCalc(path);


}

bool TSMAlgo::contains(vector<int>& currAns, int num)
{
    for(int x  = 0; x < currAns.size(); x++)
        if(currAns[x] == num)
            return true;
    return false;
}

//base of code from coding blockss, edited to check
//all routes as opposed to a guaranteed origin
//https://codingblocks.com/resources/travelling-salesman/
void TSMAlgo::dynamTSM()
{
    if(path.size() > 0)
        path.clear();

    distance = 0;


    dp.resize((int)(pow(2,n)));
    for(int x = 0; x < dp.size(); x++)
    {
        dp[x].resize(n);
    }

    for(int i=0;i<(1<<n);i++){
        for(int j=0;j<n;j++){
            dp[i][j] = -1;
        }
    }

    distance = dynamTSM(1,0);
    minDistance = distance;


    findDynamPath();

    return;
}

double TSMAlgo::dynamTSM(int mask, int pos)
{

    //2^n - 1
    int vAll = (1<<matrix.size())-1;

    int n = matrix.size();

    if(mask==vAll){
        return graph[pos][0];

    }
    if(dp[mask][pos]!=-1){
        return dp[mask][pos];

    }

    //Now from current node, we will try to go to every other node and take the min ans
    double ans = INFINITY;

    //Visit all the unvisited cities and take the best route
    for(int city = 0; city < n; city++){


        //make sure we don't check the same city against itself
        //bit mask &ed with current city must be false
        if( (mask & (1<<city)) == 0){

            double newAns = graph[pos][city] + dynamTSM( mask|(1<<city ), city);

            if(ans < newAns)
                continue;

            else
            {
                ans = newAns;
            }
        }
    }


    return dp[mask][pos] = ans;

}

void TSMAlgo::findDynamPath()
{
    int n = matrix.size();
    int previous = 0;
    double temp = distance;
    path.push_back(matrix[0].getName());

    vector<double> tracker;

    for(int x = 0; x < pow(2,n); x++)
    {
        for(int y = 0; y < n; y++)
        {
            if(dp[x][y] != -1)
            {
                tracker.push_back(dp[x][y]);
            }
        }
    }
    for(int x = 0; x < n; x++)
    {
        for(int y = 0; y < tracker.size(); y++)
        {
            if(( abs((temp - graph[previous][x]) - tracker[y]) <= .000000000000009) && (x != previous))
            {
                temp -= graph[previous][x];
                previous = x;
                x = 0;


                path.push_back(previous+1);
                if(temp == 0)
                    break;
            }
        }
        if(temp == 0)
            break;
    }


    int pathSum = 0;
    int listSum = 0;
    for(int x = 0; x < n-1; x++)
    {
        pathSum += path[x];
        listSum += matrix[x].getName();
    }
    listSum += matrix[n-1].getName();

    path.push_back(listSum-pathSum);
    //    reverse(path.begin(), path.end());

}

void TSMAlgo::display()
{
    for (int x = 0; x < path.size(); x++)
    {
        cout << path[x] << " -> ";
    }

    cout << path[0] << endl;
}

double TSMAlgo::getDistance()
{
    return distance;
}

double TSMAlgo::distanceCalc(Node& a, Node& b)
{

    return  pow(pow((a.getX() - b.getX()),2) +
                pow((a.getY() - b.getY()),2) +
                pow((a.getZ() - b.getZ()),2),.5);

}

double TSMAlgo::distanceCalc(vector<int> & path)
{
    double tempDistance = 0;
    for(int x = 0; x < path.size()-1; x++)
    {
        tempDistance += graph[path[x]-1][path[x+1]-1];
    }
    tempDistance += graph[path[path.size()-1]-1][path[0]-1];
}

//http://www.cplusplus.com/forum/unices/33379/
int TSMAlgo::factorial(int x)
{
    return (x == 1 || x == 0) ? 1 : factorial(x - 1) * x;
}

double TSMAlgo::setMaxTime(chrono::duration<double> newTime)
{
    maxAllocation = newTime;
}

double TSMAlgo::setMinTime(chrono::duration<double> newTime)
{
    minAllocation = newTime;
}

