#include "tabu.h"

tabu::tabu()
{

}

tabu::tabu(int n)
{
    length = n;
    node* head = new node;
    start = head;
    current = head;
    for(int x = 0; x < n-1; x++)
    {
        node* temp = new node;
        current->nextNode = temp;
        current = temp;
    }
    current->nextNode = head;
}

void tabu::setLength(int n)
{
    length = n;
    node* head = new node;
    start = head;
    current = head;
    for(int x = 0; x < n-1; x++)
    {
        node* temp = new node;
        current->nextNode = temp;
        current = temp;
    }
    current->nextNode = head;
}

void tabu::push(vector<int> path)
{
    current->data = path;
    current = current->nextNode;
}

bool tabu::contains(vector<int>& path)
{
    node* temp = current;
    for(int x = 0; x < length; x++)
    {
        if(current->data == path)
        {
            current = temp;
            return true;
        }
    }
    return false;

}

tabu::~tabu()
{
    while(current != start)
    {
        current = current->nextNode;
    }
    node* temp = current;
    for(int x  = 0;  x < length; x++)
    {
        node* nextTemp = temp->nextNode;
        temp->nextNode = nullptr;
        delete temp;
        temp = nextTemp;
    }
}
