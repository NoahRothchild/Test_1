#include "iostream"
#include "algorithm.h"
#include "TSM.h"
#include "string"

using namespace std;

int main(int argc, char* argv[])
{
    TSM TSMer;
    TSMer.load(0);

    for(int x = 1; x <= 4; x++)
    {
        TSMer.select(x);
        TSMer.exe();
        TSMer.display();
        TSMer.stats();
        cout << endl;
    }

    return 0;
}
