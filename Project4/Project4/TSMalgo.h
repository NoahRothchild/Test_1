#ifndef SEARCHINGALGO_H
#define SEARCHINGALGO_H
#include "vector"
#include <algorithm>
#include <iostream>
#include <bits/stdc++.h>
#include <cmath>
#include "node.h"

using namespace std;


class TSMAlgo
{
private:

    double distance;

    double minDistance;

    int n;

    vector<int> path;

    vector<Node> matrix;

    vector<vector<double>> graph;

    chrono::duration<double> maxAllocation;

    chrono::duration<double> minAllocation;

    double dynamTSM(int, int);

    vector<vector<double>> dp;

    void findDynamPath();

    double calcDynamPath(int, int);

    void genAlgoInitialization(vector<vector<int>>&);

    void selectRoulette(vector<vector<int>>&, vector<double>&, vector<int>&, vector<double> &, vector<double>&, vector<int>&);

    void selectRandom(vector<vector<int>>&, vector<double>&, vector<int>&);

    void crossover50(vector<int>&, vector<vector<int>>&, vector<int> &, vector<int> &);

    void crossoverOrder1(vector<int>&, vector<vector<int>>&, vector<int> &, vector<int> &);

    void mutationRand(vector<int>&);

    void mutationHalfRand(vector<int>&);


public:

    TSMAlgo();

    void loadPositions();

    void savePath(string);

    void forceTSM();
    void dynamTSM();
    void genAlgoTSM();


    void tabuTSM();

    void display();
    double getDistance();

    double setMaxTime(chrono::duration<double>);
    double setMinTime(chrono::duration<double>);


    double distanceCalc(Node &, Node &);
    double distanceCalc(vector<int>&);
    int factorial(int);
    bool contains(vector<int>&, int);






};



#endif // SEARCHINGALGO_H
