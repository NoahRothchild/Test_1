TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    TSMalgo.cpp \
    TSM.cpp \
    node.cpp \
    filemanager.cpp

HEADERS += \
    TSMalgo.h \
    TSM.h \
    node.h \
    filemanager.h \
    algorithm.h
