#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "string"

using namespace std;

class Algorithm
{
private:

public:

    Algorithm(){}

    //load data from a file
    virtual void load(int) = 0;

    //run the algorithm
    virtual void exe() = 0;

    //print the array to the screen
    virtual void display() = 0;

    //prints algorithm name, execution time and number of records
    virtual void stats() = 0;

    //select the type of sort used
    virtual void select(int) = 0;

    //print the array to a new file
    virtual void save(string) = 0;

    //future availabilty
    virtual void config() = 0;

    virtual ~Algorithm() {}

};



#endif // ALGORITHM_H
