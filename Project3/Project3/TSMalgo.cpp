#include "TSMalgo.h"
#include "algorithm"
#include "filemanager.h"
#include "string"

struct Node;

TSMAlgo::TSMAlgo()
{

}

void TSMAlgo::loadPositions()
{

    fileManager manager;
    matrix = manager.positionReader("positions6.txt");

    for(int x = 0; x < matrix.size(); x++)
    {
        vector<double> inserter;
        for(int y = 0; y < matrix.size(); y++)
        {
            if(matrix[x].getName() == y+1)
            {
                inserter.push_back(0);
                continue;
            }
            inserter.push_back(distanceCalc(matrix[x], matrix[y]));
        }
        graph.push_back(inserter);
    }

}

void TSMAlgo::savePath(string out)
{
    fileManager manager;
    manager.pathSaver(path, out);
}

//base of code from geeks for geeks, edited to check
//all routes as opposed to a guaranteed origin
//https://www.geeksforgeeks.org/traveling-salesman-problem-tsp-implementation/
void TSMAlgo::forceTSM()
{
    distance = 0;
    int V  = matrix.size();
    if(path.size() > 0)
        path.clear();


    // store minimum weight Hamiltonian Cycle.
    double min_path = INFINITY;

    for(int s = 0; s < matrix.size(); s++)
    {
        // store all vertex apart from source vertex
        vector<int> vertex;
        for (int i = 0; i < V; i++)
            if (i != s)
                vertex.push_back(i);


        do {

            // store current Path weight(cost)
            double current_pathweight = 0;

            vector<int> tempPath;
            tempPath.push_back(s+1);

            // compute current path weight
            int k = s;
            for (int i = 0; i < vertex.size(); i++) {
                current_pathweight += graph[k][vertex[i]];
                k = vertex[i];
                tempPath.push_back(k+1);
            }
            current_pathweight += graph[k][s];

            // update minimum
            if(current_pathweight < min_path)
            {
                path = tempPath;
                min_path = current_pathweight;
            }

        } while (next_permutation(vertex.begin(), vertex.end()));

        distance = min_path;
    }
    return;
}

//base of code from coding blockss, edited to check
//all routes as opposed to a guaranteed origin
//https://codingblocks.com/resources/travelling-salesman/
void TSMAlgo::dynamTSM()
{
    if(path.size() > 0)
        path.clear();

    distance = 0;

    int n = matrix.size();

    dp.resize((int)(pow(2,n)));
    for(int x = 0; x < dp.size(); x++)
    {
        dp[x].resize(n);
    }

    for(int i=0;i<(1<<n);i++){
        for(int j=0;j<n;j++){
            dp[i][j] = -1;
        }
    }

    distance = dynamTSM(1,0);

    findDynamPath();

    return;
}


double TSMAlgo::dynamTSM(int mask, int pos)
{

    //2^n - 1
    int vAll = (1<<matrix.size())-1;

    int n = matrix.size();

    if(mask==vAll){
        return graph[pos][0];

    }
    if(dp[mask][pos]!=-1){
        return dp[mask][pos];

    }

    //Now from current node, we will try to go to every other node and take the min ans
    double ans = INFINITY;

    //Visit all the unvisited cities and take the best route
    for(int city = 0; city < n; city++){


        //make sure we don't check the same city against itself
        //bit mask &ed with current city must be false
        if( (mask & (1<<city)) == 0){

            double newAns = graph[pos][city] + dynamTSM( mask|(1<<city ), city);

            if(ans < newAns)
                continue;

            else
            {
                ans = newAns;
            }
        }
    }


    return dp[mask][pos] = ans;

}

void TSMAlgo::findDynamPath()
{
    int n = matrix.size();
    int previous = 0;
    double temp = distance;
    path.push_back(matrix[0].getName());

    vector<double> tracker;

    for(int x = 0; x < pow(2,n); x++)
    {
        for(int y = 0; y < n; y++)
        {
            if(dp[x][y] != -1)
            {
                tracker.push_back(dp[x][y]);
            }
        }
    }
    for(int x = 0; x < n; x++)
    {
        for(int y = 0; y < tracker.size(); y++)
        {
            if(( abs((temp - graph[previous][x]) - tracker[y]) <= .000000000000009) && (x != previous))
            {
                temp -= graph[previous][x];
                previous = x;
                x = 0;


                path.push_back(previous+1);
                if(temp == 0)
                    break;
            }
        }
        if(temp == 0)
            break;
    }


    int pathSum = 0;
    int listSum = 0;
    for(int x = 0; x < n-1; x++)
    {
       pathSum += path[x];
       listSum += matrix[x].getName();
    }
    listSum += matrix[n-1].getName();

    path.push_back(listSum-pathSum);
//    reverse(path.begin(), path.end());
    cout << endl;
}

void TSMAlgo::display()
{
    for (int x = 0; x < path.size(); x++)
    {
        cout << path[x] << " -> ";
    }

    cout << path[0] << endl;
}

double TSMAlgo::getDistance()
{
    return distance;
}

double TSMAlgo::distanceCalc(Node& a, Node& b)
{

    return  pow(pow((a.getX() - b.getX()),2) +
                pow((a.getY() - b.getY()),2) +
                pow((a.getZ() - b.getZ()),2),.5);

}
