#ifndef SEARCH_H
#define SEARCH_H

#include "algorithm.h"
#include "chrono"
#include "TSMalgo.h"
#include "vector"
#include "ostream"

using namespace std;


class TSM: public Algorithm
{
private:

    int selector;

    int vertices;

    chrono::duration<double> time;

    TSMAlgo controller;

    typedef void (TSMAlgo::*fptr)();
    fptr func;


public:

    TSM(): selector{0}, vertices{0}, time{0}, controller{}
    {
        func = &TSMAlgo::forceTSM;

    }

   //load data from a file
   void load(int) override;

   //run the algorithm
   void exe() override;

   //print the array to the screen
   void display() override;

   //prints algorithm name, execution time and number of records
   void stats() override;

   //select the type of sort used
   void select(int) override;

   //print the array to a new file
   void save(string) override;

   //future availabilty
   void config() override;

   //destructor
   ~TSM();
};

#endif // SEARCH_H
