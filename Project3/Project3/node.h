#ifndef NODE_H
#define NODE_H


class Node
{
private:
    int name;

    double xPos;

    double yPos;

    double zPos;

public:
    Node();

    Node(int in, double x, double y, double z);

    int getName();

    double getX();

    double getY();

    double getZ();
};

#endif // NODE_H
