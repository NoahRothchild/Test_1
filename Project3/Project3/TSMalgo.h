#ifndef SEARCHINGALGO_H
#define SEARCHINGALGO_H
#include "vector"
#include <algorithm>
#include <iostream>
#include <bits/stdc++.h>
#include <cmath>
#include "node.h"

using namespace std;


class TSMAlgo
{
private:

    double distance;

    vector<int> path;

    vector<Node> matrix;

    vector<vector<double>> graph;

    double dynamTSM(int, int);

    vector<vector<double>> dp;

    void findDynamPath();

    double calcDynamPath(int, int);

public:

    TSMAlgo();

    void loadPositions();

    void savePath(string);

    void forceTSM();
    void dynamTSM();

    void display();
    double getDistance();

    double distanceCalc(Node &, Node &);



};



#endif // SEARCHINGALGO_H
