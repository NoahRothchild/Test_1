#include "filemanager.h"
#include "iostream"
#include "fstream"
#include "sstream"

fileManager::fileManager()
{

}

vector<Node> fileManager::positionReader(string file)
{
    vector<Node> matrix;

    ifstream inFile;
    inFile.open(file, ifstream::in);
    if (!inFile)
    {
        cerr << "open failed" << endl;
        exit(1);
    }

    while(inFile.eof() == false)
    {
        char* dest = new char[40];
        inFile.getline(dest, 40, '\n');
        string in = dest;
        delete[] dest;

        if(in == "" || in == " ")
            break;
        stringstream delimiter(in);

        int origin;

        double xPosition;
        double yPosition;
        double zPosition;

        char comma;

        delimiter >> origin;
        delimiter >> comma;
        delimiter >> xPosition;
        delimiter >> comma;
        delimiter >> yPosition;
        delimiter >> comma;
        delimiter >> zPosition;

        Node inserter(origin, xPosition, yPosition, zPosition);
        matrix.push_back(inserter);
    }

    inFile.close();
    return matrix;
}

void fileManager::pathSaver(vector<int>& path, string file)
{
    fstream output;
    output.open(file, ifstream::out);

    if(!output)
    {
        cerr << "open failed" << endl;
        exit(1);
    }

    for(int x = 0; x < path.size(); x++)
    {
        output << path[x] << endl;
    }

    output.close();

}

