#include "TSM.h"
#include "string"

void TSM::load(int a)
{
   controller.loadPositions();
}

void TSM::exe()
{

    controller.setMaxTime(longestTime);
    controller.setMinTime(shortestTime);

    //start measuring the speed
    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

    //run the algorithm through the function pointer
    (&controller->*func)();


    //stop the clock and find the time
    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
    time = chrono::duration_cast<chrono::duration<double>>(t2 - t1);

    if(time > longestTime)
        longestTime = time;

    if(time < shortestTime)
        shortestTime = time;



}

void TSM::display()
{
    controller.display();
}

void TSM::stats()
{
    cout << controller.getDistance() << " traveled\n";
    cout << time.count() << " seconds" << endl;
}

void TSM::select(int searchMethod)
{
    selector = searchMethod;
    if(selector == 1)
        func = &TSMAlgo::forceTSM;
    else if (selector == 2)
        func = &TSMAlgo::dynamTSM;
    else if (selector == 3)
        func = &TSMAlgo::genAlgoTSM;
    else if (selector == 4)
        func = &TSMAlgo::tabuTSM;
    else if (selector == 5)
        func = &TSMAlgo::simAnnTSM;
    else if (selector == 6)
        func = &TSMAlgo::partSwarmTSM;
}

void TSM::save(string out)
{
    controller.savePath(out);

}



void TSM::config()
{

}

TSM::~TSM()
{

}
