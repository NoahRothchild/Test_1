TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    TSMalgo.cpp \
    TSM.cpp \
    tabu.cpp \
    node.cpp \
    filemanager.cpp \
    particle.cpp

HEADERS += \
    TSMalgo.h \
    TSM.h \
    tabu.h \
    node.h \
    filemanager.h \
    algorithm.h \
    particle.h
