#include "node.h"

Node::Node()
{
    name = 0;
    xPos = 0;
    yPos = 0;
    zPos = 0;
}


Node::Node(int in, double x, double y, double z)
{
    name = in;
    xPos = x;
    yPos = y;
    zPos = z;
}

int Node::getName()
{
    return name;
}

double Node::getX()
{
    return xPos;
}

double Node::getY()
{
    return yPos;
}

double Node::getZ()
{
    return zPos;
}
