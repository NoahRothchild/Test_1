#include "particle.h"
#include "cmath"

particle::particle()
{
    distance = INFINITY;
}


void particle::setCurrent(vector<int>& in)
{
    current = in;
}

void particle::setPBest(vector<int>& in)
{
    pBest = in;
}

void particle::addVelocities(pair<int,int>& in)
{
    velocities.push_back(in);
}

void particle::setDistance(double in)
{

        distance = in;

}

vector<int>& particle::getCurrent()
{
    return current;
}

vector<pair<int,int>>& particle::getVelocities()
{
    return velocities;
}

vector<int>& particle::getPBest()
{
    return pBest;
}

double particle::getDistance()
{
    return distance;
}

void particle::executeSwaps()
{
    int maxVelocity = 5;
    while(velocities.size() > maxVelocity)
    {
        velocities.pop_back();
    }

    for(int x = 0; x < velocities.size(); x++)
    {
        int tempSwap = current[velocities[x].first];
        current[velocities[x].first] = current[velocities[x].second];
        current[velocities[x].second] = tempSwap;
    }
    velocities.clear();
}

