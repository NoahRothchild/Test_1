#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "node.h"
#include "vector"
#include "string"

using namespace std;

class fileManager
{
public:
    fileManager();

    vector<Node> positionReader(string);

    void pathSaver(vector<int>&, string);


};

#endif // FILEMANAGER_H
