#ifndef PARTICLE_H
#define PARTICLE_H
#include "vector"

using namespace std;

class particle
{
    vector<int> current;
    vector<int> pBest;
    vector<pair<int,int>> velocities;
    double distance;

public:
    particle();

    void setCurrent(vector<int>&);
    void setPBest(vector<int>&);
    void addVelocities(pair<int,int>&);
    void setDistance(double);

    vector<int>& getCurrent();
    vector<pair<int, int> > &getVelocities();
    vector<int>& getPBest();
    double getDistance();

    void executeSwaps();
};

#endif // PARTICLE_H
