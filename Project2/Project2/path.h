#ifndef PATH_H
#define PATH_H

#include "vector"
#include "lnode.h"
#include "mnode.h"
#include <cmath>

using namespace std;

template <class T>
class Path
{
private:

    int nodesChecked;
    double distance;
    vector<T> nodes;
    double weight;
    T start;
    T end;
    double hValue;

public:
    Path();
    Path(int,int);

    void setStart(int);
    void setEnd(int);
    void sethValue(double);
    double gethValue();
    void addNode(T);
    void push_front(T);
    void removeNode();

    void setChecked(int);
    int getChecked();
    T front();
    void pop_front();


    void clear();
    bool found(int);
    int foundAt(int);
    void calcWeight();
    double getWeight();
    void calcDistance();
    double getDistance();
    vector<T> getPath();
};

template<class T>
Path<T>::Path()
{

}

template <class T>
Path<T>::Path(int beg, int tail)
{
    start = beg;
    end = tail;
}

template <class T>
void Path<T>::setStart(int beg)
{
    start = beg;
}

template <class T>
void Path<T>::setEnd(int tail)
{
    end = tail;
}

template <class T>
void Path<T>::sethValue(double a)
{
    hValue = a;
}

template <class T>
double Path<T>::gethValue()
{
    return hValue;
}

template <class T>
void Path<T>::addNode(T newNode)
{
    nodes.push_back(newNode);

}

template <class T>
void Path<T>::push_front(T newNode)
{
    if(nodes.size()>0)
    {
        nodes.push_back(nodes[nodes.size()-1]);
        for(int x = nodes.size()-1; x > 0; x--)
        {
            nodes[x] = nodes[x-1];
        }
        nodes[0] = newNode;
    }
    else
        addNode(newNode);
}

template <class T>
void Path<T>::setChecked(int a)
{
    nodesChecked = a;
}

template <class T>
int Path<T>::getChecked()
{
    return nodesChecked;
}

template <class T>
void Path<T>::removeNode()
{
    weight -= nodes[nodes.size()-1].getWeight();
    nodes.pop_back();
}

template <class T>
double Path<T>::getWeight()
{
    return weight;
}

template <class T>
vector<T> Path<T>::getPath()
{
    return nodes;
}

template <class T>
T Path<T>::front()
{
    return nodes[0];
}

template <class T>
int Path<T>::foundAt(int in)
{
    for(int x = 0; x < nodes.size(); x++)
        if(in == nodes[x].getName())
            return x;
}

template <class T>
void Path<T>::pop_front()
{
    if(nodes.size()>1)
    {
        for(int x = 0; x < nodes.size()-1; x++)
        {
            nodes[x] = nodes[x+1];
        }
        nodes.pop_back();
    }
    else if(nodes.size() == 1)
        nodes.clear();
    else
        return;
}

template <class T>
void Path<T>::clear()
{
    nodes.clear();
}

template <class T>
bool Path<T>::found(int in)
{
    for(int x = 0; x < nodes.size(); x++)
        if(in == nodes[x].getName())
            return true;
    return false;
}

template <class T>
void Path<T>::calcWeight()
{
    weight = 0;
    for(int x = 0; x < nodes.size()-1; x++)
    {
        weight += nodes[x].getWforC(nodes[x+1].getName());
    }
}

template <class T>
double Path<T>::getDistance()
{
    return distance;
}

template <class T>
void Path<T>::calcDistance()
{
    distance = 0;
    for(int x = 0; x < nodes.size()-1; x++)
    {
        double a = (pow(nodes[x].getxPos() - nodes[x+1].getxPos(),2));
        double b = (pow(nodes[x].getyPos() - nodes[x+1].getyPos(),2));
        double c = (pow(nodes[x].getzPos() - nodes[x+1].getzPos(),2));
        distance += pow((a+b+c),.5);
    }
}


#endif // PATH_H
