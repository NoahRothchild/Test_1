#include "datafilecreator.h"
#include "iostream"
#include "stdlib.h"
#include "time.h"
#include "fstream"
#include "string"


DataFileCreator::DataFileCreator()
{
    list = {};
    size = 0;
    type = 0;
}

void DataFileCreator::createArray()
{
    //clear array
    list.clear();

    //determine array length and list style
    cout << "How many ints do you want?" << endl;
    cin >> size;
    cout << "Which type of file would you like?" << "\n";
    cout << "1) Random\n";
    cout << "2) Reversed\n";
    cout << "3) 20% Unique\n";
    cout << "4) 30% random" << endl;
    cin  >> type;


    //used to randomly swap numbers
    int swap1 = 0;
    int swap2 = 0;
    switch(type)
    {

    //Random list
    case 1:
    {
        //add numbers in order to list
        for(int x = 0; x < size; x++)
            list.push_back(x+1);

        srand(time(NULL));

        //randomly swap numbers size number of times
        for(int x = 0; x < size; x++)
        {
            swap1 = rand() % size;
            swap2 = rand() % size;
            int temp = list[swap1];
            list[swap1] = list[swap2];
            list[swap2] = temp;
        }

        //generate file with consistant name
        string fileName = "";
        string initial = "Random_";
        string num = "" + to_string(size);
        fileName = initial + num;
        fileName = fileName + ".txt";
        ofstream myfile;
        myfile.open(fileName);
        for (int x = 0; x < size; x++)
            myfile << list[x] << endl;
        myfile.close();
        break;
    }
    //Reversed List
    case 2:
    {
        //Add numbers to list in reverse order
        int counter = size;
        for(int x = 0; x < size; x++)
        {
            list.push_back(counter);
            counter--;
        }

        //generate file with consistant name
        string fileName = "";
        string initial = "Reversed_";
        string num = "" + to_string(size);
        fileName = initial + num;
        fileName = fileName + ".txt";
        ofstream myfile;
        myfile.open(fileName);
        for (int x = 0; x < size; x++)
            myfile << list[x] << endl;
        myfile.close();
        break;
    }

    // 5 Numbers randomly scattered
    case 3:
    {
        srand(time(NULL));

        //grab a random number between 1 and 5
        for(int x = 0; x < size; x++)
        {
            int temp = rand() % 5 + 1;
            list.push_back(temp);
        }

        //generate file with consistant name
        string fileName = "";
        string initial = "20Unique_";
        string num = "" + to_string(size);
        fileName = initial + num;
        fileName = fileName + ".txt";
        ofstream myfile;
        myfile.open(fileName);
        for (int x = 0; x < size; x++)
            myfile << list[x] << endl;
        myfile.close();
        break;
    }

    //30% partially sorted
    case 4:
    {
        //find 30% of the list
        int random = .3 * size;
        srand(time(NULL));

        //insert the list normally in order
        for(int x = 0; x < size; x++)
            list.push_back(x + 1);

        //swap numbers in the first 30% of the list
        for(int x = 0; x < size; x++)
        {
            swap1 = rand() % random;
            swap2 = rand() % random;
            int temp = list[swap1];
            list[swap1] = list[swap2];
            list[swap2] = temp;
        }

        //generate file with consistant name
        string fileName = "";
        string initial = "30Random_";
        string num = "" + to_string(size);
        fileName = initial + num;
        fileName = fileName + ".txt";
        ofstream myfile;
        myfile.open(fileName);
        for (int x = 0; x < size; x++)
            myfile << list[x] << endl;
        myfile.close();
        break;
    }
    }
}
