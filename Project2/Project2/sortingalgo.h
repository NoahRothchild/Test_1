#ifndef SORTINGALGO_H
#define SORTINGALGO_H

#include "vector"

//Reference GeeksforGeeks to understand different algorithms

using namespace std;

template <class T>
class sortingAlgo
{
private:

    //private functions to use during recursive calls

    //sort function
    static void mergeSort(vector<T>&, int, int);

    //merge the arrays back together
    static void merge(vector<T>&, int, int );


public:
    sortingAlgo();

    //public calls to different sorts
    static void insertionSort(vector<T>&);
    static void bubbleSort(vector<T>&);
    static void mergeSort(vector<T>&);

    ~sortingAlgo();

};

template <class T>
sortingAlgo<T>::sortingAlgo()
{

}

template <class T>
void sortingAlgo<T>::insertionSort(vector<T>& list)
{

    int temp = 0;

    //traverse all but last piece of data
    for(int x = 0; x < list.size()-1; x++)
    {
        //x piece of data to all following pieces of data
        for(int y = x; y < list.size(); y++)
        {
            //swap x and y when x is larger
            if(list[x] > list[y])
            {
                temp = list[x];
                list[x] = list[y];
                list[y] = temp;
            }
        }
    }
}

template <class T>
void sortingAlgo<T>::bubbleSort(vector<T>& list)
{

    int temp = 0;
    //iterate through array length minus 1 times
    for(int x = 0; x < list.size()-1; x++)
    {
        //travers through array in a group of 2
        //members of the group are at y and y+1
        for(int y = 0; y < list.size()-1; y++)
        {
            //swap y and y+1 when y is larger
            if(list[y] > list[y+1])
            {
                temp = list[y];
                list[y] = list[y+1];
                list[y+1] = temp;
            }
        }
    }
}

//public call to sort
//much of merge sort and its three functions are HEAVILY
//influenced from here: https://www.geeksforgeeks.org/merge-sort/
template <class T>
void sortingAlgo<T>::mergeSort(vector<T>& list)
{
    mergeSort(list, 0, list.size()-1);
}


template <class T>
void sortingAlgo<T>::merge(vector<T> &list, int left, int right)
{

    //mid point
    int mid = (left + right)/2;

    //left end point
    int lStop = mid - left + 1;

    //right end point
    int rStop = right - mid;

    //temporary arrays
    int L[lStop];
    int R[rStop];

    //fill left temp array
    for(int x = 0; x < lStop; x++)
        L[x] = list[x + left];

    //fill right temp array
    for(int x = 0; x < rStop; x++)
        R[x] = list[x + mid + 1];

    //counters
    int lIndex = 0;
    int rIndex = 0;
    int index = left;

    //while counters are smaller than stopping points
    while(lIndex < lStop && rIndex < rStop)
    {

        //if left is smaller than right
        //load left first
        if(L[lIndex] <= R[rIndex])
        {
            list[index] = L[lIndex];
            lIndex++;
            index++;
        }

        //load right before left
        else
        {
            list[index] = R[rIndex];
            rIndex++;
            index++;
        }

    }

    //enter rest of left numbers
    for(int x = lIndex; x < lStop; x++)
    {
        list[index] = L[x];
        index++;
    }

    //enter rest of right numbers
    for(int x = rIndex; x < rStop; x++)
    {
        list[index] = R[x];
        index++;
    }


}

template <class T>
void sortingAlgo<T>::mergeSort(vector<T>& list, int left, int right)
{
    //return case
    if( right <= left)
        return;

    //base case
    int mid = left + right;
    mid = mid/2;

    //call on left half
    mergeSort(list, left, mid);

    //call on right hald]f
    mergeSort(list, mid + 1, right);

    //merge halves back together
    merge(list, left, right);
}

template <class T>
sortingAlgo<T>::~sortingAlgo()
{
}


#endif // SORTINGALGO_H
