#ifndef lNode_H
#define lNode_H

#include "vector"
#include "string"
#include "node.h"

using namespace std;

class lNode: public Node
{
private:
    int name;
    vector<int> connections;
    vector<double> weights;
    int xPos;
    int yPos;
    int zPos;

public:
    lNode(): name{0}, xPos{0}, yPos{0}, zPos{0}, connections{}, weights{} {}
    lNode(int);
    bool operator==(const lNode&);
    bool operator>(const lNode&);
    void loadConnection(int);
    void loadWeight(double, int);
    void loadxPos(int);
    void loadyPos(int);
    void loadzPos(int);

    bool isConnection(int);


    int getName();
    vector<int> getConnection();
    vector<double> getWeight();
    int getWforC(int);
    int getxPos();
    int getyPos();
    int getzPos();

    void displayConnection();
    void displayWeight();
    void displayxPos();
    void displayyPos();
    void displayzPos();

    ~lNode();
};

#endif // lNode_H
