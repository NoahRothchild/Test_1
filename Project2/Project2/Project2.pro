TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    sort.cpp \
    search.cpp \
    mnode.cpp \
    datafilecreator.cpp \
    lnode.cpp

HEADERS += \
    sortingalgo.h \
    sort.h \
    searchingalgo.h \
    search.h \
    mnode.h \
    linkednode.h \
    linkedlist.h \
    datafilecreator.h \
    algorithm.h \
    path.h \
    lnode.h \
    node.h
