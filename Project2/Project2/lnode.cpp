#include "lnode.h"


lNode::lNode(int lNodeName)
{
    name = lNodeName;
}

bool lNode::operator ==(const lNode& rhs)
{
    if(name == rhs.name && xPos == rhs.xPos &&
        yPos == rhs.yPos && zPos == rhs.zPos)
        return true;
    return false;
}


void lNode::loadConnection(int in)
{
    connections.push_back(in);
    weights.push_back(0);

}

void lNode::loadWeight(double weight, int lNodeName)
{
    for(int x = 0; x < weights.size(); x++)
    {
        if(connections[x] == lNodeName)
            weights[x] = weight;
    }
}


void lNode::loadxPos(int in)
{
    xPos=(in);
}

void lNode::loadyPos(int in)
{
    yPos = (in);
}

void lNode::loadzPos(int in)
{
    zPos = (in);
}

int lNode::getName()
{
    return name;
}

vector<int> lNode::getConnection()
{
    return connections;
}

vector<double> lNode::getWeight()
{
    return weights;
}

int lNode::getWforC(int connect)
{
    for(int x = 0; x < connections.size(); x++)
    {
        if(connect == connections[x])
            return weights[x];
    }

    return 0;
}


int lNode::getxPos()
{
    return xPos;
}

int lNode::getyPos()
{
    return yPos;
}

int lNode::getzPos()
{
    return zPos;
}

bool lNode::isConnection(int in)
{
    for(int x = 0; x < connections.size(); x++)
    {
        if(in == connections[x])
            return true;
    }

    return false;
}

lNode::~lNode()
{

}
