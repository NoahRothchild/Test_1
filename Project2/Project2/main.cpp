#include "iostream"
#include "sort.h"
#include "algorithm.h"
#include "datafilecreator.h"
#include "search.h"
#include "random"

using namespace std;

int main(int argc, char* argv[])
{
    int a = atoi(argv[1]);
    int b = atoi(argv[2]);

    Search searcher(a,b);
    searcher.load(0);

    for(int x = 1; x < 13; x++)
    {
    searcher.select(x);
    searcher.exe();
    searcher.stats();
    }

    Search searcher2(b,a);
    searcher.load(0);

    for(int x = 1; x < 13; x++)
    {
    searcher.select(x);
    searcher.exe();
    searcher.stats();
    }


    return 0;
}
