#include "search.h"
#include "iostream"
#include "fstream"
#include "sstream"

void Search::load(int a)
{
    ifstream inFile;
    //make sure the file opened
    inFile.open("graph.txt", ifstream::in);
    if (!inFile)
    {
        cerr << "open failed" << endl;
        exit(1);
    }

    while(inFile.eof() == false)
    {
        char* dest = new char[40];
        inFile.getline(dest, 40, '\n');
        string in = dest;
        delete[] dest;

        stringstream delimiter(in);

        int inserter;

        delimiter >> inserter;

        char comma;
        delimiter >> comma;

        lNode temp(inserter);

        while(delimiter >> inserter)
        {
            temp.loadConnection(inserter);

            if (delimiter.peek() == ',')
                delimiter.ignore();
        }

        nodes.push_back(temp);
    }

    inFile.close();

    inFile.open("weights.txt", ifstream::in);
    if (!inFile)
    {
        cerr << "open failed" << endl;
        exit(1);
    }

    while(inFile.eof() == false)
    {
        char* dest = new char[40];
        inFile.getline(dest, 40, '\n');
        string in = dest;
        delete[] dest;

        stringstream delimiter(in);

        int origin;
        int destination;
        int weight;

        char comma;

        delimiter >> origin;
        delimiter >> comma;
        delimiter >> destination;
        delimiter >> comma;
        delimiter >> weight;

        for(int x  = 0; x < nodes.size(); x++)
        {
            if(origin == nodes[x].getName())
            {
                nodes[x].loadWeight(weight, destination);
            }
        }

    }

    inFile.close();

    inFile.open("positions.txt", ifstream::in);
    if (!inFile)
    {
        cerr << "open failed" << endl;
        exit(1);
    }

    while(inFile.eof() == false)
    {
        char* dest = new char[40];
        inFile.getline(dest, 40, '\n');
        string in = dest;
        delete[] dest;

        stringstream delimiter(in);

        int origin;

        int xPosition;
        int yPosition;
        int zPosition;

        char comma;

        delimiter >> origin;
        delimiter >> comma;
        delimiter >> xPosition;
        delimiter >> comma;
        delimiter >> yPosition;
        delimiter >> comma;
        delimiter >> zPosition;

        for(int x = 0; x < nodes.size(); x++)
        {
            if(nodes[x].getName() == origin)
            {
                nodes[x].loadxPos(xPosition);
                nodes[x].loadyPos(yPosition);
                nodes[x].loadzPos(zPosition);
                break;
            }
        }
    }

    for(int x = 0; x < nodes.size(); x++)
    {
        matrix.push_back(mNode(nodes[x].getName(), nodes));
        vertices++;
    }

    inFile.close();


}

void Search::exe()
{
    //start measuring the speed
    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

    //run the algorithm through the function pointer
    if(lOrM == 1)
        (*fptr2)((vector<Node>&)(matrix), mPath, start, end);
    else if(lOrM == 2)
        (*fptr1)((vector<Node>&)(nodes), lPath, start, end);

    //stop the clock and find the time
    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
    time = chrono::duration_cast<chrono::duration<double>>(t2 - t1);



}

void Search::display()
{

}

void Search::select(int searchMethod)
{


    if(searchMethod > 6)
    {
        selector = searchMethod - 6;
        lOrM = 2;
    }
    else
    {
        selector = searchMethod;
        lOrM = 1;
    }

    if(lOrM == 1)
    {
        switch(selector)
        {
        case 1:
            fptr2 = &searchingAlgo<Node>::iDFSm;
            break;

        case 2:
            fptr2 = &searchingAlgo<Node>::iBFSm;
            break;

        case 3:
            fptr2 = &searchingAlgo<Node>::rDFSm;
            break;

        case 4:
            fptr2 = &searchingAlgo<Node>::rBFSm;
            break;

        case 5:
            fptr2 = &searchingAlgo<Node>::Dijkstram;
            break;

        case 6:
            fptr2 = &searchingAlgo<Node>::aStarm;
            break;
        }
    }

    else if(lOrM == 2)
    {
        switch(selector)
        {
        case 1:
            fptr1 = &searchingAlgo<Node>::iDFSl;
            selector+=6;
            break;

        case 2:
            fptr1 = &searchingAlgo<Node>::iBFSl;
            selector+=6;
            break;

        case 3:
            fptr1 = &searchingAlgo<Node>::rDFSl;
            selector+=6;
            break;

        case 4:
            fptr1 = &searchingAlgo<Node>::rBFSl;
            selector+=6;
            break;

        case 5:
            fptr1 = &searchingAlgo<Node>::Dijkstral;
            selector+=6;
            break;

        case 6:
            fptr1 = &searchingAlgo<Node>::aStarl;
            selector+=6;
            break;
        }
    }
}

void Search::save(string out)
{
    ofstream myFile;
    myFile.open(out, ofstream::app);
    switch(selector)
    {
    case 1:
        myFile << "Iterative DFS on Matrix,";
        myFile << mPath.getPath()[mPath.getPath().size()-1].getName() <<",";
        myFile << mPath.getPath().size() << ",";
        mPath.calcWeight();
        mPath.calcDistance();
        myFile << mPath.getWeight() << ",";
        myFile << mPath.getDistance() << ",";
        myFile << mPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << mPath.gethValue();
        myFile << "\n";
        break;
    case 2:
        myFile << "Iterative BFS on Matrix,";
        myFile << mPath.getPath()[mPath.getPath().size()-1].getName() <<",";
        myFile << mPath.getPath().size() << ",";
        mPath.calcWeight();
        mPath.calcDistance();
        myFile << mPath.getWeight() << ",";
        myFile << mPath.getDistance() << ",";
        myFile << mPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << mPath.gethValue();
        myFile << "\n";
        break;
    case 3:
        myFile << "Recursive DFS on Matrix,";
        myFile << mPath.getPath()[mPath.getPath().size()-1].getName() <<",";
        myFile << mPath.getPath().size() << ",";
        mPath.calcWeight();
        mPath.calcDistance();
        myFile << mPath.getWeight() << ",";
        myFile << mPath.getDistance() << ",";
        myFile << mPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << mPath.gethValue();
        myFile << "\n";

        break;
    case 4:
        myFile << "Recursive BFS on Matrix,";
        myFile << mPath.getPath()[mPath.getPath().size()-1].getName() <<",";
        myFile << mPath.getPath().size() << ",";
        mPath.calcWeight();
        mPath.calcDistance();
        myFile << mPath.getWeight() << ",";
        myFile << mPath.getDistance() << ",";
        myFile << mPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << mPath.gethValue();
        myFile << "\n";

        break;
    case 5:
        myFile << "Dijkstra on Matrix,";
        myFile << mPath.getPath()[mPath.getPath().size()-1].getName() <<",";
        myFile << mPath.getPath().size() << ",";
        mPath.calcWeight();
        mPath.calcDistance();
        myFile << mPath.getWeight() << ",";
        myFile << mPath.getDistance() << ",";
        myFile << mPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << mPath.gethValue();
        myFile << "\n";

        break;
    case 6:
        myFile << "A* on Matrix,";
        myFile << mPath.getPath()[mPath.getPath().size()-1].getName() <<",";
        myFile << mPath.getPath().size() << ",";
        mPath.calcWeight();
        mPath.calcDistance();
        myFile << mPath.getWeight() << ",";
        myFile << mPath.getDistance() << ",";
        myFile << mPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << mPath.gethValue()<<",";
        myFile << "\n";

        break;
    case 7:
        myFile << "Iterative DFS on list,";
        myFile << lPath.getPath()[lPath.getPath().size()-1].getName() <<",";
        myFile << lPath.getPath().size() << ",";
        lPath.calcWeight();
        lPath.calcDistance();
        myFile << lPath.getWeight() << ",";
        myFile << lPath.getDistance() << ",";
        myFile << lPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << lPath.gethValue();
        myFile << "\n";
        break;
    case 8:
        myFile << "Iterative BFS on list,";
        myFile << lPath.getPath()[lPath.getPath().size()-1].getName() <<",";
        myFile << lPath.getPath().size() << ",";
        lPath.calcWeight();
        lPath.calcDistance();
        myFile << lPath.getWeight() << ",";
        myFile << lPath.getDistance() << ",";
        myFile << lPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << lPath.gethValue();
        myFile << "\n";
        break;
    case 9:
        myFile << "Recursive DFS on list,";
        myFile << lPath.getPath()[lPath.getPath().size()-1].getName() <<",";
        myFile << lPath.getPath().size() << ",";
        lPath.calcWeight();
        lPath.calcDistance();
        myFile << lPath.getWeight() << ",";
        myFile << lPath.getDistance() << ",";
        myFile << lPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << lPath.gethValue();
        myFile << "\n";
        break;
    case 10:
        myFile << "Recursive BFS on list,";
        myFile << lPath.getPath()[lPath.getPath().size()-1].getName() <<",";
        myFile << lPath.getPath().size() << ",";
        lPath.calcWeight();
        lPath.calcDistance();
        myFile << lPath.getWeight() << ",";
        myFile << lPath.getDistance() << ",";
        myFile << lPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << lPath.gethValue();
        myFile << "\n";
        break;
    case 11:
        myFile << "Dijkstra on list,";
        myFile << lPath.getPath()[lPath.getPath().size()-1].getName() <<",";
        myFile << lPath.getPath().size() << ",";
        lPath.calcWeight();
        lPath.calcDistance();
        myFile << lPath.getWeight() << ",";
        myFile << lPath.getDistance() << ",";
        myFile << lPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << lPath.gethValue();
        myFile << "\n";
        break;
    case 12:
        myFile << "A* on list,";
        myFile << lPath.getPath()[lPath.getPath().size()-1].getName() <<",";
        myFile << lPath.getPath().size() << ",";
        lPath.calcWeight();
        lPath.calcDistance();
        myFile << lPath.getWeight() << ",";
        myFile << lPath.getDistance() << ",";
        myFile << lPath.getChecked() << ",";
        myFile << time.count() << ",";
        if(selector == 6 || selector == 12)
            myFile << lPath.gethValue() <<",";
        myFile << "\n";
        break;
    }
}



void Search::stats()
{
    switch(selector)
    {
    case 1:
        cout << "Iterative DFS on Matrix" << endl;
        break;
    case 2:
        cout << "Iterative BFS on Matrix" << endl;
        break;
    case 3:
        cout << "Recursive DFS on Matrix" << endl;
        break;
    case 4:
        cout << "Recursive BFS on Matrix" << endl;
        break;
    case 5:
        cout << "Dijkstra on Matrix" << endl;
        break;
    case 6:
        cout << "A* on Matrix" << endl;
        break;
    case 7:
        cout << "Iterative DFS on list" << endl;
        break;
    case 8:
        cout << "Iterative BFS on list" << endl;
        break;
    case 9:
        cout << "Recursive DFS on list" << endl;
        break;
    case 10:
        cout << "Recursive BFS on list" << endl;
        break;
    case 11:
        cout << "Dijkstra on list" << endl;
        break;
    case 12:
        cout << "A* on list" << endl;
        break;
    default:
        cout << "no sort type selected" << endl;
        cout << "Iterative DFS on list" << endl;
        break;
    }

    switch(lOrM)
    {
    case 1:
        for(int x = 0; x < mPath.getPath().size()-1; x++)
            cout << mPath.getPath()[x].getName() << " -> ";
        cout << mPath.getPath()[mPath.getPath().size()-1].getName() << endl;
        cout << "Nodes Returned: " << mPath.getPath().size() << endl;
        mPath.calcWeight();
        mPath.calcDistance();
        cout << "Total Cost: " << mPath.getWeight() << endl;
        cout << "Total Distance: " << mPath.getDistance() << endl;
        cout << "Nodes Explored: " << mPath.getChecked() << endl;
        cout << "Time Taken: " << time.count() << endl;
        if(selector == 6)
            cout << "Heuristic {Sum: Distance*(1+cost)}: " << mPath.gethValue() << endl;

        break;
    case 2:
        for(int x = 0; x < lPath.getPath().size()-1; x++)
            cout << lPath.getPath()[x].getName() << " -> ";
        cout << lPath.getPath()[lPath.getPath().size()-1].getName() << endl;
        cout << "Nodes Returned: " << lPath.getPath().size() << endl;
        lPath.calcWeight();
        lPath.calcDistance();
        cout << "Total Cost: " << lPath.getWeight() << endl;
        cout << "Total Distance: " << lPath.getDistance() << endl;
        cout << "Nodes Explored: " << lPath.getChecked() << endl;
        cout << "Time Taken: " << time.count() << endl;
        if(selector == 12)
            cout << "Heuristic {Sum: Distance*(1+cost)}: " << lPath.gethValue() << endl;
        break;
    }
    cout << endl;
}

void Search::config()
{

}

Search::~Search()
{

}
