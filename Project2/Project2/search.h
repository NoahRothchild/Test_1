#ifndef SEARCH_H
#define SEARCH_H

#include "algorithm.h"
#include "lnode.h"
#include "chrono"
#include "mnode.h"
#include "searchingalgo.h"
#include "vector"
#include "node.h"
#include "path.h"
#include "ostream"

using namespace std;

class Search: public Algorithm
{
private:

    int selector;

    int lOrM;

    int vertices;

    int start;

    int end;

    chrono::duration<double> time;

    void (*fptr1)(vector<Node>&,Path<lNode>&, int, int);

    void (*fptr2)(vector<Node>&,Path<mNode>&, int, int);


    Path<Node> path;

    Path<lNode> lPath;

    Path<mNode> mPath;

    vector<lNode> nodes;

    vector<mNode> matrix;

public:

    Search(): selector{0}, lOrM{2}, vertices{0}, start{0}, end{0}, time{0},
            matrix{}, nodes{}, path{}, lPath{}, mPath{},
            fptr1{&searchingAlgo<Node>::iDFSl}, fptr2{&searchingAlgo<Node>::iDFSm}
    {


    }

    Search(int beg, int tail): selector{0}, lOrM{2}, vertices{0}, start{beg}, end{tail}, time{0},
        matrix{}, nodes{}, path{}, lPath{}, mPath{},
        fptr1{&searchingAlgo<Node>::iDFSl}, fptr2{&searchingAlgo<Node>::iDFSm}
    {

    }

    //load data from a file
   void load(int) override;

   //run the algorithm
   void exe() override;

   //print the array to the screen
   void display() override;

   //prints algorithm name, execution time and number of records
   void stats() override;

   //select the type of sort used
   void select(int) override;

   //print the array to a new file
   void save(string) override;

   void save(ofstream&);

   //future availabilty
   void config() override;

   //destructor
   ~Search();
};

#endif // SEARCH_H
