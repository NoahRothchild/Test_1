#ifndef SEARCHINGALGO_H
#define SEARCHINGALGO_H

#include "vector"
#include "mnode.h"
#include "lnode.h"
#include "path.h"
#include <algorithm>
#include <iostream>
#include <bits/stdc++.h>
#include <cmath>

using namespace std;

template <class T>
class searchingAlgo
{
private:
    static void recurseDFS(vector<T>&, Path<lNode>&, vector<bool>&, int, int);
    static void recurseDFSm(vector<T>&, Path<mNode>&, vector<bool>&, int, int);
    static void recurseBFS(vector<T>&, Path<lNode>&, vector<bool>&, vector<int>&, int, int, int);
    static void recurseBFSm(vector<T>&, Path<mNode>&, vector<bool>&, vector<int>&, int, int, int);

public:
    searchingAlgo();

    static void iDFSl(vector<T>&, Path<lNode>&, int, int);
    static void iDFSm(vector<T>&, Path<mNode>&, int, int);
    static void iBFSl(vector<T>&, Path<lNode>&, int, int);
    static void iBFSm(vector<T>&, Path<mNode>&, int, int);
    static void rDFSl(vector<T>&, Path<lNode>&, int, int);
    static void rDFSm(vector<T>&, Path<mNode>&, int, int);
    static void rBFSl(vector<T>&, Path<lNode>&, int, int);
    static void rBFSm(vector<T>&, Path<mNode>&, int, int);

    static void Dijkstral(vector<T>&, Path<lNode>&, int, int);
    static void Dijkstram(vector<T>&, Path<mNode>&, int, int);
    static void aStarl(vector<T>&, Path<lNode>&, int, int);
    static void aStarm(vector<T>&, Path<mNode>&, int, int);

    static int distanceCalcl(lNode&, lNode&);
    static int distanceCalcm(mNode& , mNode& );
};

template <class T>
searchingAlgo<T>::searchingAlgo()
{

}

//reference used https://gist.github.com/douglas-vaz/5072998
template <class T>
void searchingAlgo<T>::iDFSl(vector<T>& graph, Path<lNode>& answer, int start, int end)
{
    int check = 0;
    answer.clear();
    vector<lNode>* ptr = &((vector<lNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);

    Path<lNode> stack;
    Path<lNode> path;

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            stack.addNode((*ptr)[x]);
            break;
        }

    while(stack.getPath().size() != 0)
    {
        lNode temp = stack.front();
        path.addNode(temp);
        stack.pop_front();
        check++;

        if(temp.getName() == end)
        {
            path.setChecked(check);
            answer = path;
            return;
        }

        vector<lNode> children;

        for(int x = 0; x < temp.getConnection().size(); x++)
        {
            children.push_back((*ptr)[temp.getConnection()[x]-1]);

        }
        std::reverse(children.begin(), children.end());

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                stack.push_front(children[x]);
                visited[children[x].getName()-1] = true;
            }
        }
    }
    path.setChecked(check);
    answer = path;

    return;

}

template <class T>
void searchingAlgo<T>::iDFSm(vector<T>& graph, Path<mNode>& answer, int start, int end)
{
    int check = 0;
    answer.clear();
    vector<mNode>* ptr = &((vector<mNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);

    Path<mNode> stack;
    Path<mNode> path;

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            stack.addNode((*ptr)[x]);
            break;
        }

    while(stack.getPath().size() != 0)
    {
        mNode temp = stack.front();
        path.addNode(temp);
        stack.pop_front();
        check++;

        if(temp.getName() == end)
        {
            path.setChecked(check);
            answer = path;
            return;
        }

        vector<mNode> children;

        for(int x = 0; x < temp.getMatrix().size(); x++)
        {
            if(temp.getMatrix()[x] > 0)
            {
                children.push_back((*ptr)[x]);
            }

        }
        std::reverse(children.begin(), children.end());

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                stack.push_front(children[x]);
                visited[children[x].getName()-1] = true;
            }
        }
    }
    answer = path;
    return;

}

//reference used https://gist.github.com/douglas-vaz/5072998
template <class T>
void searchingAlgo<T>::iBFSl(vector<T>& graph, Path<lNode>& answer, int start, int end)
{
    int check = 0;
    answer.clear();
    vector<lNode>* ptr = &((vector<lNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);
    vector<int> parent;

    for(int x = 0; x < (*ptr).size(); x++)
        parent.push_back(0);

    Path<lNode> queue;
    Path<lNode> path;

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            parent[x] = 0;
            queue.push_front(((*ptr)[x]));
            break;
        }


    while(queue.getPath().size() != 0)
    {
        lNode temp = queue.front();
        path.addNode(temp);
        queue.pop_front();
        check++;


        if(temp.getName() == end)
        {
            //            answer = path;
            Path<lNode> truePath;
            temp = path.getPath()[path.getPath().size()-1];
            truePath.addNode(temp);
            while(truePath.front().getName() != start)
            {
                temp = (*ptr)[parent[temp.getName()-1]-1];
                truePath.push_front(temp);
            }
            truePath.setChecked(check);
            answer = truePath;
            return;
        }

        vector<lNode> children;

        for(int x = 0; x < temp.getConnection().size(); x++)
        {
            children.push_back((*ptr)[temp.getConnection()[x]-1]);

        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                queue.addNode(children[x]);
                visited[children[x].getName()-1] = true;
                parent[children[x].getName()-1] = temp.getName();
            }
        }
    }
    answer = path;
    return;

}

template <class T>
void searchingAlgo<T>::iBFSm(vector<T>& graph, Path<mNode>& answer, int start, int end)
{
    int check = 0;
    answer.clear();
    vector<mNode>* ptr = &((vector<mNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);
    vector<int> parent;

    for(int x = 0; x < (*ptr).size(); x++)
        parent.push_back(0);

    Path<mNode> queue;
    Path<mNode> path;

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            parent[x] = 0;
            queue.push_front(((*ptr)[x]));
            break;
        }


    while(queue.getPath().size() != 0)
    {
        mNode temp = queue.front();
        path.addNode(temp);
        queue.pop_front();
        check++;


        if(temp.getName() == end)
        {
            //            answer = path;
            Path<mNode> truePath;
            temp = path.getPath()[path.getPath().size()-1];
            truePath.addNode(temp);
            while(truePath.front().getName() != start)
            {
                temp = (*ptr)[parent[temp.getName()-1]-1];
                truePath.push_front(temp);
            }
            truePath.setChecked(check);
            answer = truePath;
            return;
        }

        vector<mNode> children;

        for(int x = 0; x < temp.getMatrix().size(); x++)
        {
            if(temp.getMatrix()[x] > 0)
                children.push_back((*ptr)[x]);

        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                queue.addNode(children[x]);
                visited[children[x].getName()-1] = true;
                parent[children[x].getName()-1] = temp.getName();
            }
        }
    }
    answer = path;
    return;
}

template <class T>
void searchingAlgo<T>::rDFSl(vector<T>& graph, Path<lNode>& answer, int start, int end)
{
    answer.clear();
    vector<lNode>* ptr = &((vector<lNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);

    Path<lNode> path;
    answer.setChecked(0);

    recurseDFS(graph, answer, visited, start, end);

}

template <class T>
void searchingAlgo<T>::recurseDFS(vector<T> & graph, Path<lNode> & answer, vector<bool> & visited, int source, int end)
{
    answer.setChecked(answer.getChecked()+1);
    int currentNode = 0;
    vector<lNode>* ptr = &((vector<lNode>&)(graph));
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == source)
        {
            currentNode = x;
            visited[x] = true;
            answer.addNode((*ptr)[x]);
            break;
        }

    if(answer.getPath()[answer.getPath().size()-1].getName() == end)
        return;

    else
    {
        lNode temp = answer.getPath()[answer.getPath().size()-1];

        vector<lNode> children;

        for(int x = 0; x < temp.getConnection().size(); x++)
        {
            children.push_back((*ptr)[temp.getConnection()[x]-1]);

        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                visited[children[x].getName()-1] = true;
                return recurseDFS(graph, answer, visited, children[x].getName(), end);
            }
        }

    }

}

template <class T>
void searchingAlgo<T>::rDFSm(vector<T>& graph, Path<mNode>& answer, int start, int end)
{
    answer.clear();
    vector<mNode>* ptr = &((vector<mNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);

    Path<mNode> path;

    answer.setChecked(0);

    recurseDFSm(graph, answer, visited, start, end);
}

template <class T>
void searchingAlgo<T>::recurseDFSm(vector<T> & graph, Path<mNode> & answer, vector<bool> & visited, int source, int end)
{
    answer.setChecked(answer.getChecked()+1);
    int currentNode = 0;
    vector<mNode>* ptr = &((vector<mNode>&)(graph));
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == source)
        {
            currentNode = x;
            visited[x] = true;
            answer.addNode((*ptr)[x]);
            break;
        }

    if(answer.getPath()[answer.getPath().size()-1].getName() == end)
        return;

    else
    {
        mNode temp = answer.getPath()[answer.getPath().size()-1];

        vector<mNode> children;

        for(int x = 0; x < temp.getMatrix().size(); x++)
        {
            if(temp.getMatrix()[x] > 0)
                children.push_back((*ptr)[x]);

        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                visited[children[x].getName()-1] = true;
                return recurseDFSm(graph, answer, visited, children[x].getName(), end);
            }
        }

    }
}

template <class T>
void searchingAlgo<T>::rBFSl(vector<T>& graph, Path<lNode>& answer, int start, int end)
{
    answer.clear();
    vector<lNode>* ptr = &((vector<lNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);
    vector<int> parent;

    for(int x = 0; x < (*ptr).size(); x++)
        parent.push_back(0);


    Path<lNode> path;


    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            //            parent[x] = 0;
            answer.addNode(((*ptr)[x]));
            break;
        }

    answer.setChecked(0);

    recurseBFS(graph, answer, visited, parent, start, end, 0);

    Path<lNode> truePath;
    lNode temp = answer.getPath()[answer.getPath().size()-1];
    truePath.addNode(temp);
    truePath.setChecked(answer.getChecked());
    while(truePath.front().getName() != start)
    {
        temp = (*ptr)[parent[temp.getName()-1]-1];
        truePath.push_front(temp);
    }
    answer = truePath;
    return;
}


template <class T>
void searchingAlgo<T>::rBFSm(vector<T>& graph, Path<mNode>& answer, int start, int end)
{
    answer.clear();
    vector<mNode>* ptr = &((vector<mNode>&)(graph));

    vector<bool> visited;
    for(int x  = 0; x < (*ptr).size(); x++)
        visited.push_back(false);
    vector<int> parent;

    for(int x = 0; x < (*ptr).size(); x++)
        parent.push_back(0);


    Path<mNode> path;


    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            //            parent[x] = 0;
            answer.addNode(((*ptr)[x]));
            break;
        }

    answer.setChecked(0);
    recurseBFSm(graph, answer, visited, parent, start, end, 0);

    Path<mNode> truePath;
    mNode temp = answer.getPath()[answer.foundAt(end)];
    truePath.addNode(temp);
    truePath.setChecked(answer.getChecked());
    while(truePath.front().getName() != start)
    {
        temp = (*ptr)[parent[temp.getName()-1]-1];
        truePath.push_front(temp);
    }
    answer = truePath;
    return;
}

template <class T>
void searchingAlgo<T>::recurseBFS(vector<T> & graph, Path<lNode> & answer, vector<bool> & visited, vector<int>& parent, int source, int end, int location)
{
    answer.setChecked(answer.getChecked()+1);
    vector<lNode>* ptr = &((vector<lNode>&)(graph));

    if(answer.found(end))
        return;

    else
    {
        lNode temp = answer.getPath()[location];
        location++;

        vector<lNode> children;

        for(int x = 0; x < temp.getConnection().size(); x++)
        {
            children.push_back((*ptr)[temp.getConnection()[x]-1]);

        }


        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                parent[children[x].getName()-1] = temp.getName();
                visited[children[x].getName()-1] = true;
                answer.addNode(children[x]);

            }
        }


        return recurseBFS(graph, answer, visited, parent, answer.getPath()[location].getName(), end, location);

    }
}

template <class T>
void searchingAlgo<T>::recurseBFSm(vector<T> & graph, Path<mNode> & answer, vector<bool> & visited, vector<int>& parent, int source, int end, int location)
{
    answer.setChecked(answer.getChecked()+1);
    vector<mNode>* ptr = &((vector<mNode>&)(graph));

    if(answer.found(end))
        return;

    else
    {
        mNode temp = answer.getPath()[location];
        location++;

        vector<mNode> children;

        for(int x = 0; x < temp.getMatrix().size(); x++)
        {
            if(temp.getMatrix()[x] > 0)
                children.push_back((*ptr)[x]);

        }


        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                parent[children[x].getName()-1] = temp.getName();
                visited[children[x].getName()-1] = true;
                answer.addNode(children[x]);

            }
        }


        return recurseBFSm(graph, answer, visited, parent, answer.getPath()[location].getName(), end, location);

    }

}

template <class T>
void searchingAlgo<T>::Dijkstral(vector<T>& graph, Path<lNode>& answer, int start, int end)
{
    answer.clear();
    answer.setChecked(0);
    vector<lNode>* ptr = &((vector<lNode>&)(graph));


    vector<int> tracker((*ptr).size(), INFINITY);
    tracker[start-1] = 0;

    vector<bool> visited((*ptr).size(), false);
    vector<int> parent((*ptr).size(), 0);

    Path<lNode> queue;
    Path<lNode> path;
    path.setChecked(0);

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            queue.push_front(((*ptr)[x]));
            break;
        }


    while(queue.getPath().size() != 0)
    {
        lNode temp = queue.front();
        path.addNode(temp);
        queue.pop_front();
        path.setChecked(path.getChecked()+1);


        vector<lNode> children;

        for(int x = 0; x < temp.getConnection().size(); x++)
        {
            children.push_back((*ptr)[temp.getConnection()[x]-1]);
            int tempWeight = (*ptr)[temp.getName()-1].getWforC(children[x].getName());

            if(tracker[children[x].getName()-1] == INFINITY)
                tracker[children[x].getName()-1] = tempWeight;

            else if(tempWeight + tracker[temp.getName()-1] < tracker[children[x].getName()-1])
            {
                parent[children[x].getName()-1] = temp.getName();
                tracker[children[x].getName()-1] = tempWeight + tracker[temp.getName()-1];
            }
        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                queue.addNode(children[x]);
                visited[children[x].getName()-1] = true;
                parent[children[x].getName()-1] = temp.getName();
            }
        }


    }

    path.clear();
    path.addNode((*ptr)[end-1]);
    int parentalIndex = parent[end-1];
    while(path.front().getName() != start)
    {
        path.push_front((*ptr)[parentalIndex-1]);
        parentalIndex = parent[parentalIndex - 1];
    }

    answer = path;



}


template <class T>
void searchingAlgo<T>::Dijkstram(vector<T>& graph, Path<mNode>& answer, int start, int end)
{
    answer.clear();

    vector<mNode>* ptr = &((vector<mNode>&)(graph));


    vector<int> tracker((*ptr).size(), INFINITY);
    tracker[start-1] = 0;

    vector<bool> visited((*ptr).size(), false);
    vector<int> parent((*ptr).size(), 0);

    Path<mNode> queue;
    Path<mNode> path;
    path.setChecked(0);

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            queue.push_front(((*ptr)[x]));
            break;
        }


    while(queue.getPath().size() != 0)
    {
        mNode temp = queue.front();
        path.addNode(temp);
        queue.pop_front();
        path.setChecked(path.getChecked()+1);


        vector<mNode> children;

        for(int x = 0; x < temp.getMatrix().size(); x++)
        {
            if(temp.getMatrix()[x] > 0)
            {
                children.push_back((*ptr)[x]);
            }
        }

        for(int x = 0; x < children.size(); x++)
        {
            int tempWeight = (*ptr)[temp.getName()-1].getWforC(children[x].getName());
            if(tracker[children[x].getName()-1] == INFINITY)
                tracker[children[x].getName()-1] = tempWeight;
            else if(tempWeight + tracker[temp.getName()-1] < tracker[children[x].getName()-1])
            {
                parent[children[x].getName()-1] = temp.getName();
                tracker[children[x].getName()-1] = tempWeight + tracker[temp.getName()-1];
            }
        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                queue.addNode(children[x]);
                visited[children[x].getName()-1] = true;
                parent[children[x].getName()-1] = temp.getName();
            }
        }


    }

    path.clear();
    path.addNode((*ptr)[end-1]);
    int parentalIndex = parent[end-1];
    while(path.front().getName() != start)
    {
        path.push_front((*ptr)[parentalIndex-1]);
        parentalIndex = parent[parentalIndex - 1];
    }

    answer = path;


}




template <class T>
void searchingAlgo<T>::aStarl(vector<T>& graph, Path<lNode>& answer, int start, int end)
{
    answer.clear();
    vector<lNode>* ptr = &((vector<lNode>&)(graph));

    vector<double> tracker((*ptr).size(),INFINITY);
    tracker[start-1] = 0;

    vector<int> distances((*ptr).size(), 0);
    for(int x = 0; x < (*ptr).size(); x++)
    {
        distances[x] = (distanceCalcl((*ptr)[x], (*ptr)[end-1]));
    }

    vector<bool> visited((*ptr).size(), false);
    vector<int> parent((*ptr).size(), 0);

    Path<lNode> queue;
    Path<lNode> path;
    path.setChecked(0);

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            queue.push_front(((*ptr)[x]));
            break;
        }



    while(queue.getPath().size() != 0)
    {
        lNode temp = queue.front();
        path.addNode(temp);
        queue.pop_front();
        path.setChecked(path.getChecked()+1);

        vector<lNode> children;
        for(int x = 0; x < temp.getConnection().size(); x++)
        {
            children.push_back((*ptr)[temp.getConnection()[x]-1]);

            int tempWeight = (1+(*ptr)[temp.getName()-1].getWforC(children[x].getName()))*(distances[children[x].getName()-1]);

            if(tracker[children[x].getName()-1] == INFINITY)
                tracker[children[x].getName()-1] = tempWeight;

            else if(tempWeight + tracker[temp.getName()-1] < tracker[children[x].getName()-1])
            {
                parent[children[x].getName()-1] = temp.getName();
                tracker[children[x].getName()-1] = tempWeight + tracker[temp.getName()-1];
            }
        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                queue.addNode(children[x]);
                visited[children[x].getName()-1] = true;
                parent[children[x].getName()-1] = temp.getName();
            }
        }
    }
    path.clear();
    path.addNode((*ptr)[end-1]);
    int parentalIndex = parent[end-1];
    while(path.front().getName() != start)
    {
        path.push_front((*ptr)[parentalIndex-1]);
        parentalIndex = parent[parentalIndex - 1];
    }


    answer = path;


}


//unfurtunately I was not able to correct my a* for my matrix class
template <class T>
void searchingAlgo<T>::aStarm(vector<T>& graph, Path<mNode>& answer, int start, int end)
{
    answer.clear();
    vector<mNode>* ptr = &((vector<mNode>&)(graph));


    vector<int> tracker((*ptr).size(), 0);
    for(int x = 0; x < (*ptr).size(); x++)
    {
        tracker[x] = distanceCalcm((*ptr)[x], (*ptr)[end-1]);
    }

    vector<bool> visited((*ptr).size(), false);
    vector<int> parent((*ptr).size(), 0);

    Path<mNode> stack;
    Path<mNode> path;
    path.setChecked(0);

    int currentNode = 0;
    for(int x = 0; x < (*ptr).size(); x++)
        if((*ptr)[x].getName() == start)
        {
            currentNode = x;
            visited[x] = true;
            stack.push_front(((*ptr)[x]));
            break;
        }



    while(stack.getPath().size() != 0)
    {
        mNode temp = stack.front();
        path.addNode(temp);
        stack.pop_front();
        path.setChecked(path.getChecked()+1);

        if(temp.getName() == end)
        {
            answer = path;
            return;
        }

        vector<pair<mNode&, int>> heuros;
        vector<mNode> children;

        for(int x = 0; x < temp.getMatrix().size(); x++)
        {
            if(temp.getMatrix()[x] > 0)
                children.push_back((*ptr)[x]);

        }

        for (int x = 0; x < children.size(); ++x)
        {
            if(visited[children[x].getName()-1] == false)
            {
                pair<mNode&, int> tempInsert(children[x], (distanceCalcm(children[x], temp) + tracker[children[x].getName()-1]));
                heuros.push_back(tempInsert);
            }
        }


        if(heuros.size() > 1)
        {
            for(int x = 0; x < heuros.size()-1; x++)
            {
                for(int y = x; y < heuros.size(); y++)
                {
                    if(heuros[x].second > heuros[y].second)
                    {
                        pair<mNode,int> tempSwitch = heuros[x];
                        heuros[x] = heuros[y];
                        heuros[y] = tempSwitch;
                    }
                }
            }
        }

        for(int x = 0; x < heuros.size(); x++)
        {
            stack.push_front(heuros[x].first);
            visited[heuros[x].first.getName()-1] = true;
        }




    }
}

template <class T>
int searchingAlgo<T>::distanceCalcl(lNode& a, lNode& b)
{
    return   pow((a.getxPos() - b.getxPos()),2) +
            pow((a.getyPos() - b.getyPos()),2) +
            pow((a.getzPos() - b.getzPos()),2);

}

template <class T>
int searchingAlgo<T>::distanceCalcm(mNode& a, mNode& b)
{
    return   pow((a.getxPos() - b.getxPos()),2) +
            pow((a.getyPos() - b.getyPos()),2) +
            pow((a.getzPos() - b.getzPos()),2);

}

#endif // SEARCHINGALGO_H
