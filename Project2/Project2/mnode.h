#ifndef MNODE_H
#define MNODE_H

#include "vector"
#include "string"
#include "lnode.h"
#include "node.h"

using namespace std;

class mNode: public Node
{
    int name;
    vector<double> matrix;
    int xPos;
    int yPos;
    int zPos;
    double hValue;

public:
    mNode(): name{0}, xPos{0}, yPos{0}, zPos{0}, matrix{} {}
    mNode(int, vector<lNode>&);
    bool operator==(const lNode&);
    bool operator>(const lNode&);
    void loadxPos(int);
    void loadyPos(int);
    void loadzPos(int);
    void loadName(int);
    void loadWeight(int, int);

    void sethValue(double);
    double gethValue();
    double getWforC(int);
    vector<double>& getMatrix();
    int getName();
    int getxPos();
    int getyPos();
    int getzPos();

    void displayMatrix();
    void displayxPos();
    void displayyPos();
    void displayzPos();
    ~mNode();
};

#endif // MNODE_H
