#ifndef DATAFILECREATOR_H
#define DATAFILECREATOR_H

#include "vector"

using namespace std;

class DataFileCreator
{
private:

    //data array
    vector<int> list;

    //data list length
    int size;

    //type of list wanted
    int type;
public:


    DataFileCreator();

    //menu to create information
    void createArray();


};

#endif // DATAFILECREATOR_H
