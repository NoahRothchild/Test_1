#include "mnode.h"



mNode::mNode(int nameIn, vector<lNode>& entries)
{
    name = nameIn;

    for(int x = 0; x < entries.size(); x++)
    {
        matrix.push_back(0);
    }


    for(int x = 0; x < entries.size(); x++)
    {
        if(name == entries[x].getName())
        {
            vector<int> tempCon = entries[x].getConnection();
            vector<double> tempWeight = entries[x].getWeight();
            for(int y = 0; y < tempCon.size(); y++)
            {
               matrix[tempCon[y]-1] = tempWeight[y];
            }
            xPos = entries[x].getxPos();
            yPos = entries[x].getyPos();
            zPos = entries[x].getzPos();
        }
    }
}

int mNode::getName()
{
    return name;
}

int mNode::getxPos()
{
    return xPos;
}


int mNode::getyPos()
{
    return yPos;
}

int mNode::getzPos()
{
    return zPos;
}

vector<double>& mNode::getMatrix()
{
    return matrix;
}

double mNode::getWforC(int in)
{
    return matrix[in-1];
}

void mNode::sethValue(double a)
{
    hValue = a;
}

double mNode::gethValue()
{
    return hValue;
}

mNode::~mNode()
{

}
