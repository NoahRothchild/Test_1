TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    sort.cpp \
    datafilecreator.cpp

HEADERS += \
    sort.h \
    algorithm.h \
    sortingalgo.h \
    datafilecreator.h
