#include "sort.h"
#include "iostream"
#include "fstream"
#include "sstream"



void Sort::load(int file){

    //ensure our list is empty
    list.clear();

    //intialize the file
    filePath = fileNames[file];
    ifstream inFile;

    //make sure the file opened
    inFile.open(filePath.c_str(), ifstream::in);
    if (!inFile)
    {
        cerr << "open failed" << endl;
        exit(1);
    }

    //find the length of the file
    //made for the file name to contain the length as
    //last digits following a "_"
    try
    {
    int chars = filePath.find(".") - filePath.find("_") - 1;
    num = stoi(filePath.substr(filePath.find("_")+1, chars));
    }

    //if the file isnt named with my standards, count the number of entries
    catch (...)
    {
        num = 0;
        int tempCatch = 0;
        while(inFile.eof() == false)
        {
            inFile >> tempCatch;
            num++;
        }
        num--;
        inFile.clear();
        inFile.seekg(0, ios::beg);
    }


    //read in from the file
    int temp = 0;
    for(int x  = 0; x < num; x++)
    {
        inFile >> temp;
        list.push_back(temp);
    }

    //close the file
    inFile.close();

}

void Sort::exe()
{
    //start measuring the speed
    chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

    //run the algorithm through the function pointer
    (*fptr)(list);

    //stop the clock and find the time
    chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
    time = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
}

void Sort::display()
{
    //print the array to the screen
    for(int x = 0; x < num; x++)
        cout << list[x] << endl;
}

void Sort::stats()
{
    //identify what type of sort I used
    switch(selector)
    {
    case 1:
        cout << "Insertion Sort" << endl;
        break;
    case 2:
        cout << "Merge Sort" << endl;
        break;
    case 3:
        cout << "Bubble Sort" << endl;
        break;
    default:
        cout << "no sort type selected" << endl;
        cout << "default is bubble sort" << endl;
        break;
    }

    //print the data size and the time taken
        cout << num << " data points" << endl;
        cout << time.count() << " seconds" << endl;
        cout << endl;

}

void Sort::select(int sortMethod)
{
    //set calss selector
    selector = sortMethod;

    //change destination of class function pointer
    switch(selector)
    {
    case 1:
        fptr = &sortingAlgo<int>::insertionSort;
        break;
    case 2:
        fptr = &sortingAlgo<int>::mergeSort;
        break;
    case 3:
        fptr = &sortingAlgo<int>::bubbleSort;
        break;
    }
}

void Sort::save(string outPath)
{
    //create and open file to print to
    fstream output;
    output.open(outPath, ifstream::out);

    //ensure file opene
    if(!output)
    {
        cerr << "open failed" << endl;
        exit(1);
    }

    //print to file
    for(int x = 0; x < num; x ++)
    {
        output << list[x] << endl;
    }

    //close file
    output.close();
}

void Sort::config()
{
    cout << "future expandability" << endl;
}



Sort::~Sort(){

}
