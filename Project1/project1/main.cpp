#include "iostream"
#include "sort.h"
#include "algorithm.h"
#include "datafilecreator.h"

using namespace std;

int main(int argc, char* argv[])
{
    Sort search;
    //x determines the sorting method
    //1 is insertion
    //2 is merge
    //3 is bubble
    for(int x = 1; x < 4; x++)
    {
        //y grabs file names.
        //assumes that file 0 is the name of the program
        for(int y = 0; y < 16; y++)
        {
            search.load(y);
            search.select(x);
            search.exe();
            search.stats();
        }
    }


//    //Code used to create new files of varying sizes
//    DataFileCreator x;
//    x.createArray();

    return 0;
}
