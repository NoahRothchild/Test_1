#ifndef SORT_H
#define SORT_H

#include "algorithm.h"
#include "vector"
#include "chrono"
#include "sortingalgo.h"


using namespace std;

class Sort: public Algorithm
{
private:

    //array of numbers
    vector<int> list;

    //length of array
    int num;

    //int used to identify wanted sort
    int selector;

    //read in file name
    string filePath;

    //execution time of a sort
    chrono::duration<double> time;

    //function pointer used in execution
    void (*fptr)(vector<int>&);

    //Vector of my file names
    vector<string> fileNames;


public:

    //default constructor
    Sort(): list{}, selector{0}, filePath{}, time{0}, fptr{&sortingAlgo<int>::bubbleSort}
    {
        //file names
        fileNames.push_back("Random_10.txt");
        fileNames.push_back("Random_1000.txt");
        fileNames.push_back("Random_10000.txt");
        fileNames.push_back("Random_100000.txt");
        fileNames.push_back("Reversed_10.txt");
        fileNames.push_back("Reversed_1000.txt");
        fileNames.push_back("Reversed_10000.txt");
        fileNames.push_back("Reversed_100000.txt");
        fileNames.push_back("20Unique_10.txt");
        fileNames.push_back("20Unique_1000.txt");
        fileNames.push_back("20Unique_10000.txt");
        fileNames.push_back("20Unique_100000.txt");
        fileNames.push_back("30Random_10.txt");
        fileNames.push_back("30Random_1000.txt");
        fileNames.push_back("30Random_10000.txt");
        fileNames.push_back("30Random_100000.txt");
    }

     //load data from a file
    void load(int) override;

    //run the algorithm
    void exe() override;

    //print the array to the screen
    void display() override;

    //prints algorithm name, execution time and number of records
    void stats() override;

    //select the type of sort used
    void select(int) override;

    //print the array to a new file
    void save(string) override;

    //future availabilty
    void config() override;

    //destructor
    ~Sort();


};

#endif // SORT_H
